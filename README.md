## A Quarto Manuscript Template

This is a template repo for generating a manuscript from Quarto that accompanies the tutorial at: [Quarto Manuscripts: RStudio](https://quarto.org/docs/manuscripts/authoring/rstudio.html)

I would like to use it as a template for scientific manuscripts in Medical Education.